from django.contrib.auth.views import LoginView, LogoutView
from django.views.generic.base import TemplateView
from django.views.generic import ListView
from .mixins import CoreAuthMixin
from django.urls import reverse_lazy
from django.shortcuts import redirect
from cultivo.models import Cultivo, Sensor, DataSensor
from usuarios.models import DatosPersonales
# Create your views here.


class LoginHomeView(LoginView):
    """Login y presentacion"""
    template_name = "core/home.html"


class HomePageView(CoreAuthMixin, ListView):
    """Inicio"""
    template_name = "core/data.html"
    model = Cultivo
    
    def get_queryset(self):
        usuario = self.request.user
        if DatosPersonales.objects.filter(usuario=usuario):
            datospersonales = DatosPersonales.objects.get(usuario=usuario)
            return Cultivo.objects.filter(propietario=datospersonales)
    
    def get_context_data(self, **kwargs):
        datasensor = super().get_context_data(**kwargs)
        usuario = self.request.user
        propietario = DatosPersonales.objects.get(usuario=usuario)
        if Cultivo.objects.filter(propietario=propietario):
            print('si hay cultivo')
            cultivo = Cultivo.objects.get(propietario=propietario)
            sensor = Sensor.objects.get(imei=cultivo.sensor)
            if DataSensor.objects.filter(sensor=sensor):
                print('si hay sensor data')
                #senosrdata = DataSensor.objects.get(sensor=sensor)
                senosrdata = DataSensor.objects.filter(sensor=sensor).reverse()[0]
                datasensor['data_sensor'] = senosrdata
                return datasensor
            else:
                print('no hay sensor data')
                pass
        else:
            print('no hay cultivo')
            pass
        #return datasensor

    def dispatch(self, request, *args, **kwargs):
        usuario = self.request.user
        if DatosPersonales.objects.filter(usuario=usuario).exists():
            return super(HomePageView, self).dispatch(request, *args, **kwargs)
        else:
            return redirect('create_datospersonales')
    

class SignOutView(LogoutView):
    """Cerrar secion"""
    pass

