from django.db import models
from django.utils.timezone import now
from usuarios.models import DatosPersonales

# Create your models here.
class Sensor(models.Model):
    latidude = models.FloatField( blank=True, null=True, verbose_name='Latitude')
    longitude = models.FloatField(blank=True, null=True, verbose_name='Longitude')
    imei = models.CharField(max_length=15, primary_key=True, verbose_name='IMEI')
    created = models.DateTimeField(auto_now_add=True, verbose_name="Fecha de creación")
    updated = models.DateTimeField(auto_now=True, verbose_name="Fecha de edición")

    class Meta:
        verbose_name = "Sensor"
        verbose_name_plural = "Sensores"

    def __str__(self):
        return "{}".format(self.imei)


class DataSensor(models.Model):
    sensor = models.ForeignKey(Sensor, verbose_name="Sensor", on_delete=models.CASCADE)
    humedad_ambiental = models.FloatField(verbose_name="Humedad ambiental")
    temperatura_ambiental = models.FloatField(verbose_name="Temperatura ambiental")
    suelo_uno_hum = models.FloatField(verbose_name="Suelo uno Humedad")
    suelo_uno_tem = models.FloatField(verbose_name="Suelo uno temperatura")
    suelo_uno_con = models.FloatField(verbose_name="Suelo uno conductividad")
    suelo_dos_hum = models.FloatField(verbose_name="Suelo dos humedad")
    suelo_dos_tem = models.FloatField(verbose_name="Suelo dos temperatura")
    suelo_dos_con = models.FloatField(verbose_name="Suelo dos conductividad")
    hoja_hum = models.FloatField(verbose_name="Hoja Humedad")
    hoja_temp = models.FloatField(verbose_name="Hoja temperatura")
    luminosidad = models.FloatField(verbose_name="Luminosidad")
    velocidad_viento = models.FloatField(verbose_name="Velocidad del viento")
    direccion_viento = models.CharField(max_length=10, verbose_name="Direccion del viento")
    radiacion_solar = models.FloatField(verbose_name="Radiacion solar")
    created = models.DateTimeField(auto_now_add=True, verbose_name="Fecha de creación")


    class Meta:
        verbose_name = "Data Sensor"
        verbose_name_plural = "Data Sensores"

    def __str__(self):
        return "{}".format(self.sensor)



class Cultivo(models.Model):
    nombre = models.CharField(max_length=200, verbose_name="Nombre del cultivo")
    descripcion = models.TextField(verbose_name="Descripcion")
    siembra_published = models.DateTimeField(verbose_name="Fecha de siembra", default=now)
    sensor = models.ForeignKey(Sensor, verbose_name="Sensor", on_delete=models.CASCADE)
    propietario = models.OneToOneField(DatosPersonales, verbose_name="Propietario", on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True, verbose_name="Fecha de creación")
    updated = models.DateTimeField(auto_now=True, verbose_name="Fecha de edición")


    class Meta:
        verbose_name = "Cultivo"
        verbose_name_plural = "Cultivos"

    def __str__(self):
        return "{}".format(self.nombre)