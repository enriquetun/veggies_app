from django.contrib import admin
from .models import Sensor, DataSensor, Cultivo
# Register your models here.


class SensorAdmin(admin.ModelAdmin):
    list_display = ('imei',)


class DataSensorAdmin(admin.ModelAdmin):
    readonly_fields = ('created',)
    list_display = ('sensor',)


class CultivoAdmin(admin.ModelAdmin):
    list_display = ('nombre', 'siembra_published')


admin.site.register(Sensor, SensorAdmin)
admin.site.register(DataSensor, DataSensorAdmin)
admin.site.register(Cultivo, CultivoAdmin)
